# Bhav copy project

BSE publishes Bhav Copy data every day, for Equity and other segments.
The Bhav Copy shows information related to shares traded that day and
other common info. such as Open, High, Low, Close, Volumes traded,.. etc.
This project intends to show the latest bhav copy in a web UI.
The web application also has functionality to download latest Bhav Copy, search for stocks and display results.
It makes use of Redis to store data in the server. CherryPy web application to display
results in the front-end.

## Installation
Everything is dockerized. Follow these three steps to get the application running.
### Prerequisites - You must have docker installed.
### 1. Clone the project

```bash
git clone https://bitbucket.org/sumitj39/bhavcopy.git
cd bhavcopy # go into cloned bhavcopy directory
# Give the execute permission to install.sh file
chmod +x install.sh
```
### 2. Build the images
```bash
./install.sh build
```
### 3. Start the docker containers
```bash
./install.sh start
```

## Usage
use ```./install.sh start``` to start the docker containers. ```./install.sh stop``` to stop them. ```./install.sh help``` for help and usage.
### 4. The web application can be accessed at [http://127.0.0.1:8080/](http://127.0.0.1:8080/) once you start it.

### 8. Open the link and click on ``` download bhavcopy``` to download latest data.

## Developer notes
If you would like to change the configuration of redis, edit the ```redis.conf``` file and redeploy.  
To change web server's port, edit the config in app.py file. A comment has been added to show how to change the port.  
This also needs change in containers.yml file(ports: section in yml needs to be changed).    

## Issues
For issues related to installation and usage, please mail me at [sumit_jogalekar@outlook.com](mailto:sumit_jogalekar@outlook.com)
