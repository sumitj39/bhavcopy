import logging
import sys
from datetime import datetime
from pathlib import Path

import pandas as pd
import redis

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

DOWNLOAD_DIR = str(Path("../data/").resolve())


def decode(dobj, format='UTF-8'):
    """
    safe decode. if bytes object, decodes. else it will return as it is.
    :param dobj: object to be decoded
    :param format: utf-8 by default. Included to make it generic
    :return: decoded object or object itself
    """
    if hasattr(dobj, 'decode'):
        return dobj.decode(format)
    else:
        return dobj


def csv2redis(csvfile, ddate=None, rds=None):
    """
        this function reads a CSV file, converts it into pandas dataframe
        And stores file contents to redis database.
    :param csvfile: File from which to read data. Bhav copy csv file
    :param ddate: date for which bhavcopy was downloaded
    :param rds: redis instance. Can be set to None.
    :return: returns None.
    """
    # instatiate redis if not already present
    if not isinstance(rds, redis.Redis):
        rds = redis.Redis()
    # try converting ddate to datetime object.
    # throws error if wrong values passed
    if not isinstance(ddate, datetime):
        ddate = datetime.today()
    # csv expects ['SC_CODE', 'SC_NAME', "OPEN", "HIGH", "LOW", "CLOSE"] in the file.
    try:
        print(csvfile)
        csv = pd.read_csv(csvfile, usecols=['SC_CODE', 'SC_NAME', "OPEN", "HIGH", "LOW", "CLOSE"])
    except Exception as e:
        logging.error("Exception during reading the file:")
        logging.error(e)
        return
    logging.info(csv.head())
    strdate = ddate.strftime("%Y%m%d")
    """
        the below snippet stores csv data into redis datastructures
        Three DS have been used:
        1. sorted set(ZSET) - To store symbols with ranks(according to how they appear in csv files)
        2. Lists: key->symbol, values->[symbol_code, open, high, low, close]
        3. key->value for storing the date of bhavcopy
        Before appending values to set(rpush), delete existing key and then delete.
        This ensures we have data for only one day. 
    """
    with rds.pipeline() as pipeline:
        for rank, row in enumerate(csv.iterrows(), start=1):
            r1 = row[1]  # gives the pd.Series object, [0] is index, [1] is series
            symbol = str(r1['SC_NAME']).strip()  # csv has extra spaces
            pipeline.delete(symbol)  # Safe op. if key doesnt exist, wont throw exception
            pipeline.zrem("symbols", symbol)  # safe op.
            # add fresh entries
            pipeline.rpush(symbol, *[r1['SC_CODE'], r1['OPEN'], r1['HIGH'], r1['LOW'], r1['CLOSE']])
            pipeline.zadd('symbols', {symbol: rank})
        pipeline.set('date', ddate.strftime("%Y-%m-%d"))
        pipeline.execute()


def redis2pd(rds, lower, upper):
    """
    method to test functionality of redis read
    :param rds: redis instance
    :param lower: first entry index to fetch
    :param upper: last entry index to fetch
    :return: pd.DataFrame
    """
    symbols = rds.zrange("symbols", lower, upper)
    data_redis = []
    data = []
    with rds.pipeline() as pipeline:
        for symbol in symbols:
            key = decode(symbol)
            pipeline.lrange(key, 0, -1)
        data_redis = pipeline.execute()
    for s, d in zip(symbols, data_redis):
        # aggregate symbol and value
        cohlc = [decode(s)] + [decode(i) for i in d]
        data.append(cohlc)
    try:
        df = pd.DataFrame(data, columns=['symbol', 'code', 'open', 'high', 'low', 'close'])
    except Exception as e:
        # Can happen when data is not there for any entries, or not contained for some entries
        logging.error(e)
        return None
    return df


def search_symbols(rds, pattern):
    """
    Given a pattern, returns all stock symbols found in redis
    :param rds: redis.Redis connection
    :param pattern: String, eg: HDFC will match HDFC, HDFC BANK, HDFC AMC...
    :return: list of matched symbols
    """
    cursor, symbols = rds.zscan("symbols", cursor=0, match="*" + pattern + "*", count=rds.zcard("symbols"))
    return [decode(i[0]) for i in symbols]


def get_stocks(rds, symbols):
    """
    Gets list of stocks for all symbols in a pandas DataFrame
    :param rds: redis connection
    :param symbols: list of symbols to be obtained
    :return: pd.Dataframe containing list of stocks and their details
    """
    data_redis = []
    data = []
    with rds.pipeline() as pipeline:
        for symbol in symbols:
            symbol = decode(symbol)
            pipeline.lrange(symbol, 0, -1)  # fetch stock info for that symbol
        data_redis = pipeline.execute()
    for s, d in zip(symbols, data_redis):
        # aggregate symbol and value
        cohlc = [decode(s)] + [decode(i) for i in d]
        data.append(cohlc)
    try:
        df = pd.DataFrame(data, columns=['symbol', 'code', 'open', 'high', 'low', 'close'])
    except Exception as e:
        # Can happen when data is not there for any entries, or not contained for some entries
        logging.error(e)
        return None
    return df


# function to dry test redis apis
def rwrite(r):
    r.flushdb()
    csv2redis(DOWNLOAD_DIR + "/" + "EQ230819.csv", datetime(2019, 8, 23), r)
    logging.info(r.lrange('20190823_TCS LTD.', 0, -1))
    logging.info(r.zrange('symbols', 0, 9))
    r.bgsave()


# function to dry test redis apis
def rread(r):
    redis2pd(r, 0, 9)
    logging.info(r.lrange('20190823_TCS LTD.', 0, -1))
    logging.info(r.smembers('symbols'))


if __name__ == '__main__':
    # TEST
    r = redis.Redis()
    rwrite(r)
