import requests
import os
from datetime import datetime
from pathlib import Path
from zipfile import ZipFile
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

BC_EQUITY_URL = "https://www.bseindia.com/download/BhavCopy/Equity/EQ{0}_CSV.ZIP"
BC_EQISIN_URL = "https://www.bseindia.com/download/BhavCopy/Equity/EQ_ISINCODE_{0}.zip"
BC_DERIVATIVE_URL = "https://www.bseindia.com/download/Bhavcopy/Derivative/bhavcopy22-08-19.zip"
DOWNLOAD_DIR = str(Path("data/").resolve())


def download_bc(eqtype='EQ', ddate=None):
    """
    Downloads bhavcopy for type 'eqtype' for date 'ddate'
    :param eqtype: Equity types supported in BSE website
    :param ddate: date for which Bhavcopy is being downloaded
    :return: file name of downloaded file or exception when request fails.
    """
    if not ddate:
        # if ddate is None, set default of today
        ddate = datetime.today()
    if not isinstance(ddate, datetime):
        raise TypeError("date must be of type datetime.datetime")
    # extend the if else clause to add extra types here.
    if eqtype == 'EQ':
        url = BC_EQUITY_URL.format(ddate.strftime("%d%m%y"))
    elif eqtype == 'EQISIN':
        url = BC_EQISIN_URL.format(ddate.strftime("%d%m%y"))
    else:
        raise TypeError("type must be one of: {0}".format(["EQ", "EQISIN"]))
    logging.debug("Requesting: {0}".format(url))
    resp = requests.get(url)
    logging.debug("Response: {0}".format(resp))
    if resp.ok:
        # to handle 404
        # downloaded file name format will be "EQ_ddmmyy.zip
        fname = DOWNLOAD_DIR + "/" + "{0}_{1}.zip".format(eqtype, ddate.strftime("%d%m%y"))
        with open(fname, "wb") as f:
            f.write(resp.content)
        return fname
    else:
        # raise exception or return ""
        logging.error("url: {0} does not exist".format(url))
        raise Exception("Could not download Bhav Copy")


def unzip_bc(fname):
    """
    Unzips a bhavcopy file and extracts it to DOWNLOAD_DIR
    :param fname: file name to be extracted (*.zip)
    :return: None
    """
    try:
        zip = ZipFile(fname)
        zip.extractall(DOWNLOAD_DIR)
    except Exception as e:
        logging.error("Could not extract.")
        logging.error(e)
        raise e


def download_unzip_bc(eqtype='EQ', ddate=None):
    """
    calls download and unzip utilities.
    :param eqtype: Equity type supported in BSE Bhavcopy website
    :param ddate: date for which Bhavcopy is being downloaded
    :return: returns csv file extracted from bhavcopy
    """
    logging.info("Downloading Bhav Copy")
    fname = download_bc(eqtype, ddate)
    if fname:
        logging.info("Extracting zip file {0}".format(fname))
        unzip_bc(fname)
        if ddate is None:
            ddate = datetime.today().strftime("%d%m%y")
        else:
            ddate = ddate.strftime("%d%m%y")
        # For EQ type, extracted csv will be EQ<ddmmyyyy>.csv
        return os.path.dirname(fname) + "/" + eqtype + ddate + ".CSV"


if __name__ == '__main__':
    # TEST
    logging.info(download_unzip_bc('EQ', datetime(2019, 8, 23)))
