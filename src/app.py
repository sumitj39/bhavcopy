import os.path
import cherrypy
import jinja2
import redis
import datetime
import bseconnector
import redisdb
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.INFO)


def convert_df_to_lists(pd_stocks):
    """
    converts dataframe object into symbols and stock dictionary
    columns must be [symbol, code, open, high, low, close
    :param pd_stocks:
        :return: (symbols:list, stocks:dict)
    """
    ssymbols, stocks = [], {}
    for row in pd_stocks.iterrows():
        row = row[1]  # row[0] is the index of dataframe
        # df contains ['symbol', code, open, high, low, close]
        ssymbols.append(row['symbol'])
        stocks[row['symbol']] = row['code'], row['open'], row['high'], row['low'], row['close']
    return ssymbols, stocks


def decode(dobj, format='UTF-8'):
    """
    safe decode. if bytes object, decodes. else it will return as it is.
    :param dobj: object to be decoded
    :param format: utf-8 by default. Included to make it generic
    :return: decoded object or object itself
    """
    if hasattr(dobj, 'decode'):
        return dobj.decode(format)
    else:
        return dobj


class BhavCopyServer(object):

    def __init__(self):
        self.r = redis.Redis(host='redis', port=6379, db=0, password='protected')

    @cherrypy.expose
    def index(self):
        """
        Index/Home page for bhavcopy webpage.
        Lists first 10 entries from bhavcopy.
        :return: template for index page.
        """
        date = self.r.get("date")
        symbols, stocks = [], {}
        ddate = ""
        if date:
            date = decode(date)
            date = datetime.datetime.strptime(date, "%Y-%m-%d").strftime("%Y%m%d")
            ddate = datetime.datetime.strptime(date, "%Y%m%d").strftime("%d-%m-%Y")
            try:
                pd_stocks = redisdb.redis2pd(self.r, 0, 9)
            except Exception as e:
                logging.error("Could not get data from redis")
                logging.error(e)
            if pd_stocks is not None:
                symbols, stocks = convert_df_to_lists(pd_stocks)
        # stocks = [["INFY", 50001, 700, 720, 690, 710], ["TCS", 50002, 600, 620, 590, 610]]
        return jinja2.Template(open('templates/index.html').read()).render(stocks=stocks, symbols=symbols, date=ddate)

    @cherrypy.expose
    def downloadbcopy(self):
        """
        Downloads the BhavCopy from BSE website
        If given date is not available (eg: saturday/sunday/holiday)
        will go upto 5 days back.
        :return: template for index
        """
        day = datetime.datetime.today()
        logging.info(day)
        downloaded = False
        """
        Iterate 5 times to fetch data for different dates.
        once data is found for a day,
        1. Delete old data,
        2. Store latest data
        3. Save to redis database
        """
        for i in range(5):
            try:
                csvfile = bseconnector.download_unzip_bc('EQ', day)
                logging.info(csvfile)
                self.r.flushdb()
                redisdb.csv2redis(csvfile, day, self.r)
                self.r.save()
                downloaded = True
                logging.info("Downloaded")
                return self.index()
            except redis.exceptions.ResponseError:
                pass  # Redis save already in progress, so ignore
            except Exception as e:
                day = day - datetime.timedelta(1)
        if not downloaded:
            logging.error("Could not find even in 5 attempts")
            message = "Could not download Bhav Copy"
            return jinja2.Template(open("templates/error.html").read()).render(message=message)

    @cherrypy.expose
    def searchstock(self, symbol):
        """
        url:<host>/searchstock?symbol=<symbol>.
        Searches for partial match of symbol in redis symbol list
        :param symbol: pattern to be searched
        :return: jinja2 html template
        """
        symbol = str(symbol).upper()  # capitalize it so that it matches with redis keys
        date = self.r.get('date')  # get the date stored in redis
        if not date:
            # redis returns None when no data
            message = "Could not find matching results for <i>{0}</i>".format(symbol)
            return jinja2.Template(open("templates/error.html").read()).render(message=message)
        date = decode(date)
        date = datetime.datetime.strptime(date, "%Y-%m-%d").strftime("%Y%m%d")  # convert from 2019-08-23 to 20190823
        ddate = datetime.datetime.strptime(date, "%Y%m%d").strftime("%d-%m-%Y")  # convert from 20190823 to 23-08-2019
        logging.info("Got {0}".format(symbol))
        ssymbols, stocks = [], {}
        try:
            # Select all the symbols that match *symbol* pattern,
            # count is set to length of zset so that all symbols are retrieved
            symbols = redisdb.search_symbols(self.r, "*" + symbol + "*")
            pd_stocks = redisdb.get_stocks(self.r, symbols)
        except Exception as e:
            # in case of error, return an error message.
            # can happen when redis is not up.
            logging.error("Exception occured while communicating with redis")
            logging.error(e)
            message = "Could not find matching results for <i>{0}</i>".format(symbol)
            return jinja2.Template(open("templates/error.html").read()).render(message=message)
        if pd_stocks is not None:
            ssymbols, stocks = convert_df_to_lists(pd_stocks)
        if stocks:
            return jinja2.Template(open('templates/index.html').read()).render(stocks=stocks, symbols=ssymbols,
                                                                               date=ddate, searching=True,
                                                                               search_string=symbol)
        else:
            message = "Could not find matching results for <i>{0}</i>".format(symbol)
            return jinja2.Template(open("templates/error.html").read()).render(message=message)


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd()),
        },
    }
    cherrypy.config.update({'server.socket_host': '0.0.0.0'})
    # Uncomment this if running in a production environment.
    """
        cherrypy.config.update({'server.socket_host': '0.0.0.0',
                        'server.socket_port': 80,
                       })
    """
    cherrypy.quickstart(BhavCopyServer(), '/', conf)
