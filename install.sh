#!/bin/bash

DOCKER_YML=${DOCKER_YML:-containers.yml}


db () {
     echo "building $1" &&
     echo "docker build -f "Dockerfile"  --tag "$1" ." &&
     docker build -f "Dockerfile"  --tag "$1" .
}


build() {
    cd 'redis-docker' && db redisdb
    cd '../src/' && db bhavcopy

}

start() {
    docker-compose -f ${DOCKER_YML} up -d
    docker ps
}

stop() {
    docker-compose -f ${DOCKER_YML} stop
    docker-compose -f ${DOCKER_YML} rm -f
    docker ps
}

case "$1" in
    build)
        build
        ;;
    start)
        start
        ;;
    stop)
        stop
        ;;
    *)
        echo $"Usage: ./install.sh {start|stop|build}"
        RETVAL=2
        ;;
esac

exit $RETVAL